/******************************************************************************
* Author:   Marko Štajcer
* Created:	12th April 2017
* Purpose:  Show charts (Chart.js and Flot) based on data from web service
*****************************************************************************/

/* chart colors default */
var $chrt_border_color = "#efefef";
var $chrt_grid_color = "#DDD"
var $chrt_main = "#E24913";
/* red       */
var $chrt_second = "#6595b4";
/* blue      */
var $chrt_third = "#FF9F01";
/* orange    */
var $chrt_fourth = "#7e9d3a";
/* green     */
var $chrt_fifth = "#BD362F";
/* dark red  */
var $chrt_mono = "#000";

var randomScalingFactor = function() {
	return Math.round(Math.random() * 100);
};
var randomColorFactor = function() {
	return Math.round(Math.random() * 255);
};
var randomColor = function(opacity) {
	return 'rgba(' + randomColorFactor() + ',' + randomColorFactor() + ',' + randomColorFactor() + ',' + (opacity || '.3') + ')';
};

var consentsChart = function() {
    $.ajax({
        url: "GetConsentsGraph",
        contentType: 'application/json; charset=utf-8',
        type: 'GET',
        dataType: 'json',
        error: function (xhr, status) {
            alert(status);
        },
        success: function (jdata) {
            var d = jdata;
            
            $.plot("#consentschart", [{
                data: d,
                label: "Consents",
                bars: {
                    show: true,
                    barWidth: 0.2,
                    align: "center"
                }
            }], 
            {
                series: {
                    bars: {
                        show: true,
                        barWidth: 0.2,
                        align: "center"
                    }
                },
                xaxis: {
                    mode: "categories",
                    tickLength: 0
                },
                colors : [$chrt_second, $chrt_fourth, "#666", "#BBB"],
                grid : {
                    show : true,
                    hoverable : true,
                    clickable : true,
                    tickColor : $chrt_border_color,
                    borderWidth : 0,
                    borderColor : $chrt_border_color
                },
                legend : true,
                tooltip : true,
                tooltipOpts : {
                    content : "<b>Number of Consents</b> = <span>%y</span>",
                    defaultTheme : false
                }
            });
        }
    });
};


var RTBFGraph = function() {
    $.ajax({
        url: "GetRTBFGraph",
        contentType: 'application/json; charset=utf-8',
        type: 'GET',
        dataType: 'json',
        error: function (xhr, status) {
            alert(status);
        },
        success: function (jdata) {
            console.log(jdata);
            
            var months = [];
            var totalCnt=[];
            
            $.each(jdata, function(index, field) {
                months.push(field[0]);
                totalCnt.push(field[1]); 
            });
            
            var LineConfig = {
                type: 'line',
                data: {
                    labels: months,
                    datasets: [
                        {
                            label: "RTBF Requests",
                            fillColor: "rgba(220,220,220,0.2)",
                            strokeColor: "rgba(220,220,220,1)",
                            pointColor: "rgba(220,220,220,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(220,220,220,1)",
                            data: totalCnt 
                        }
                    ]
                },
                options: {
                    legend: {
                        display: false
                    },
                    responsive: true,
                    maintainAspectRatio: false,

                    tooltips: {
                        mode: 'label'
                    },
                    hover: {
                        mode: 'dataset'
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                show: true,
                                labelString: 'Month'
                            }
                        }],
                        yAxes: [{
                            display: true,
                            scaleLabel: {
                                show: true,
                                labelString: 'Number'
                            },
                            ticks: {
                                suggestedMin: 0,
                                suggestedMax: 100,
                            }
                        }]
                    }
                }
            };
            
            $.each(LineConfig.data.datasets, function(i, dataset) {
                dataset.borderColor = 'rgba(0,0,0,0.15)';
                dataset.backgroundColor = randomColor(0.5);
                dataset.pointBorderColor = 'rgba(0,0,0,0.15)';
                dataset.pointBackgroundColor = randomColor(0.5);
                dataset.pointBorderWidth = 1;
            });
            
            if(window.myChart) {
                window.myChart.destroy();
            }
                
            window.myChart = new Chart(document.getElementById("rtbfChart"), LineConfig);
        }
    });
};

var DataPortabilityGraph = function() {
    $.ajax({
        url: "DataPortabilityGraph",
        contentType: 'application/json; charset=utf-8',
        type: 'GET',
        dataType: 'json',
        error: function (xhr, status) {
            alert(status);
        },
        success: function (jdata) {
            var d = jdata;
            
            $.plot("#dataPortablilitysChart", [{
                data: d,
                label: "Data Portability",
                bars: {
                    show: true,
                    barWidth: 0.2,
                    align: "center"
                }
            }], 
            {
                series: {
                    bars: {
                        show: true,
                        barWidth: 0.2,
                        align: "center"
                    }
                },
                xaxis: {
                    mode: "categories",
                    tickLength: 0
                },
                colors : [$chrt_second, $chrt_fourth, "#666", "#BBB"],
                grid : {
                    show : true,
                    hoverable : true,
                    clickable : true,
                    tickColor : $chrt_border_color,
                    borderWidth : 0,
                    borderColor : $chrt_border_color
                },
                legend : true,
                tooltip : true,
                tooltipOpts : {
                    content : "<b>Number of Data Portability</b> = <span>%y</span>",
                    defaultTheme : false
                }
            });
        }
    });
};

$(document).ready(function() { 
   // DO NOT REMOVE : GLOBAL FUNCTIONS!
   pageSetUp(); 
   
   consentsChart();
   
   RTBFGraph();
   
   DataPortabilityGraph();
   
});

 