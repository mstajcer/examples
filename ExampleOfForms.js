/******************************************************************************
* Author:   Marko Štajcer
* Created:	13th April 2017
* Purpose:  Add entries for consent through the form
*****************************************************************************/

runAllForms();

var errorClass = 'invalid';
var errorElement = 'em';

$(function () {
    pageSetUp();

    // Validation
    $("#consent-form").validate({
        errorClass: errorClass,
        errorElement: errorElement,
        highlight: function (element) {
            $(element).parent().removeClass('state-success').addClass("state-error");
            $(element).removeClass('valid');
        },
        unhighlight: function (element) {
            $(element).parent().removeClass("state-error").addClass('state-success');
            $(element).addClass('valid');
        },
        // Rules for form validation
        rules: {
            consentName: {
                required: true
            },
            type: {
                required: true
            },
            level: {
                required: true
            }
        },

        // Messages for form validation
        messages: {
            consentName: {
                required: 'Enter the Consent Name',
            },
            type: {
                required: 'Enter type'
            },
            email: {
                required: 'Enter level'
            }
        },
        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });

});

function addConsent() {
    if ($("#consent-form").valid()) {
        submitConsentData($("#consentName").val(), 
			$("#consentDescription").val(),
            $('select#type option:checked').val(), 
			$('select#level option:checked').val(),
            $("#note").val(), 
		null);
    }
}

function submitConsentData(consentName, consentDescription, type, level, note) {
    $("#btn_addConsent").addClass("disabled");
    $("#btn_addConsent").html('<i class="jobState fa fa-gear fa-spin fa-lg"></i>');

    $.ajax({
        method: "POST",
        url: "AddConsent",
        data: {
            consentName: consentName,
            consentDescription: consentDescription,
            type: type,
            level: level,
            note: note
        },
        error: function (xhr, status) {
            //debugger;
            $(".alert-warning").fadeIn(1000);
            $("#errorMsg").html("Error while updating data. Details: " + status);
            $(".alert-warning").fadeOut(2000);
        },
        success: function (response) {
            $(".alert-success").fadeIn(1000);
            $("#successMsg").html("Consent" + response + " added.");
            $(".alert-success").fadeOut(2000);
        }
    });
    $("#btn_addConsent").removeClass("disabled");
    $("#btn_addConsent").html('Adding Consent...');
}

