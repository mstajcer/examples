/******************************************************************************
* Author:   Marko Štajcer
* Created:	20th April 2017
* Purpose:  Show charts (Morris.js) based on data from web services
* Params: 	Inputs - Property set containing a list of inputs
*			Outputs - Property set containing a list of outputs
* Returns:  Outputs
*****************************************************************************/

var selectedDate = null;

function MorrisItem() {
    this.label = "";
    this.value = 0;
};

/* Bar chart shows content items in 24 hours */
var contentItemsIn24Hours = function () {
    $.ajax({url: "rest/GetDailyWatchDurationByItem?date_from=" + moment(selectedDate).format('YYYY-MM-DD'),
        type: 'GET',
        dataType: 'json',
        error: function (xhr, status) {
            debugger;
            alert(status);
        },
        success: function (jdata) {
            var items = jdata.data;
            window.bar = NaN;
            var bar = Morris.Bar({
                element: 'contentItemsBarChart',
                grid: false,
                //quick fix for missing labels 
                xLabelAngle: 10,
                horizontal: true,
                xLabelMargin: 10,
                data: items,
                xkey: ['content_item_title'],
                ykeys: ['total_watch_duration_seconds'],
                labels: ['Totals'],
                barColors: function (row, series, type) {
                    return '#CC0B10';
                }
            }).on('click', function (i, row) {
                $('#actions-donut').html("");
                $('#device-donut').html("");
                $('#os-donut').html("");
                $('#delivery-donut').html("");
                $('#average-donut').html("");

                var date = row.date_from;
                var content_item_id = row.content_item_id;
                console.log(content_item_id);

                //get actions by content_item_id and date and draw actions donut chart
                $.ajax({url: "rest/GetDailyActionsByItem?date_from=" + moment(date).format('YYYY-MM-DD') + "&content_item_id=" + content_item_id,
                    contentType: 'application/json; charset=utf-8',
                    type: 'GET',
                    dataType: 'json',
                    error: function (xhr, status) {
                        alert(status);
                    },
                    success: function (detailsdata) {
                        console.log(detailsdata.data);
                        var jdata=detailsdata.data;
                        var arr = [];//debugger;
                        
                        for (var z = 0; z < jdata.length; z++) {
                            arr.push([jdata[z].action_name, jdata[z].action_count]);
                        }
                        function plotNow() {
                            var d = [];
                            d.push(datak[0]);
                            if (d.length > 0) {
                                if (plot2) {
                                    plot2.setData(d);
                                    plot2.draw();
                                } else {
                                    plot2 = $.plot(target, d, options);
                                }
                            }
                        };
                        trgt = arr;
                        target = $("#actions-donut");
                        var datak = [{
                                label: "Actions count",
                                data: trgt,
                                bars: {
                                    show: true,
                                    align: "center", barWidth: 0.5
                                }
                            }]
                        var options = {
                            grid: {
                                hoverable: true, clickable: true
                            },
                            tooltip: true,
                            tooltipOpts: {
                                defaultTheme: true
                            },
                            xaxis: {
                                mode: "categories"
                            },
                            yaxes: {
                                tickFormatter: function (val, axis) {
                                    return "$" + val;
                                },
                                max: 1200
                            }
                        };
                        plot2 = null;
                        plotNow();
                    }
                });

                // TO DO: -->
                //get devices by content_item_id, date 
                //and draw devices donut chart
                $.ajax({url: "rest/daily_devices?date_from=" + moment(date).format('YYYY-MM-DD') + "&content_item_id=" + content_item_id,
                    contentType: 'application/json; charset=utf-8',
                    type: 'GET',
                    dataType: 'json',
                    error: function (xhr, status) {
                        alert(status);
                    },
                    success: function (detailsdata) {
                        console.log(detailsdata.data);
                        var arr = [];
                        for (var i = 0; i < detailsdata.data.length; i++)
                        {
                            var item = new MorrisItem();
                            item.label = detailsdata.data[i].device_type_name;
                            item.value = detailsdata.data[i].device_type_count;
                            arr.push(item);
                        }
                        if ($('#device-donut').length) {
                            Morris.Donut({
                                element: 'device-donut',
                                parseTime: false,
                                data: arr,
                                formatter: function (x) {
                                    return x;
                                }
                            });

                        }
                    }
                });

                //get operating systems by content_item_id and date
                //and draw operating system donut
                $.ajax({url: "rest/daily_operating_system?date_from=" + moment(date).format('YYYY-MM-DD') + "&content_item_id=" + content_item_id,
                    contentType: 'application/json; charset=utf-8',
                    type: 'GET',
                    dataType: 'json',
                    error: function (xhr, status) {
                        alert(status);
                    },
                    success: function (detailsdata) {
                        console.log(detailsdata.data);
                        var arr = [];
                        for (var i = 0; i < detailsdata.data.length; i++)
                        {
                            var item = new MorrisItem();
                            item.label = detailsdata.data[i].operating_system_name;
                            item.value = detailsdata.data[i].operating_system_count;
                            arr.push(item);
                        }
                        if ($('#os-donut').length) {
                            Morris.Donut({
                                element: 'os-donut',
                                parseTime: false,
                                data: arr,
                                formatter: function (x) {
                                    return x;
                                }
                            });

                        }
                    }
                });

                //get content delivery type by item
                //and draw donut chart
                $.ajax({url: "rest/daily_delivery?date_from=" + moment(date).format('YYYY-MM-DD') + "&content_item_id=" + content_item_id,
                    contentType: 'application/json; charset=utf-8',
                    type: 'GET',
                    dataType: 'json',
                    error: function (xhr, status) {
                        alert(status);
                    },
                    success: function (detailsdata) {
                        console.log(detailsdata.data);
                        var arr = [];
                        for (var i = 0; i < detailsdata.data.length; i++)
                        {
                            var item = new MorrisItem();
                            item.label = detailsdata.data[i].delivery_type_name;
                            item.value = detailsdata.data[i].delivery_type_count;
                            arr.push(item);
                        }
                        if ($('#delivery-donut').length) {
                            Morris.Donut({
                                element: 'delivery-donut',
                                parseTime: false,
                                data: arr,
                                formatter: function (x) {
                                    return x;
                                }
                            });
                        }
                    }
                });

                // average donut graph
                if ($('#average-donut').length) {
                    Morris.Donut({
                        element: 'average-donut',
                        data: [{label: "Average Duration", value: row.average_watch_duration_seconds}],
                        formatter: function (x) {
                            return Math.round(x, 0) + " seconds";
                        }
                    });
                }

            }); // end of on click event handler
        } // end of success part of the master ajax call
    });
};

$(document).ready(function () {

	//hardcoded date for testing
    var defaultDate = new Date(2016, 9, 10);
    contentItemsIn24Hours(defaultDate);

    $("#datetimepicker10").datepicker({changeMonth: true, changeYear: true, buttonImageOnly: true, prevText: "<",
        nextText: ">"}).on("change", function (e) {
        $('#actions-donut').html("");
        $('#device-donut').html("");
        $('#os-donut').html("");
        $('#delivery-donut').html("");
        $('#average-donut').html("");
        $('#contentItemsBarChart').html("");

        selectedDate = e.target.value;
        contentItemsIn24Hours();
    });

    $('#datetimepicker10').datepicker("setDate", new Date(2016, 9, 10));

});

