package com.pi.conqweb;

import com.pi.controls.ChartMetadataColumn;
import com.google.gson.Gson;
import com.pi.controls.ChartData;
import com.pi.controls.PiCassandraResultsetIterator;
import com.pi.controls.PiVerticaResultsetIterator;
import com.pi.conq.utils.JsonBuilder;
import domain.ServerInstance;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.QueryParam;
/**
 * 
 * @author mastajcer
 */
@Path("/GetDailyTopItems")
public class GetDailyTopItems {

    @Context
    private UriInfo context;

    public GetDailyTopItems() {
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    /**
     * Returns data for the most watched content items based on the selected date and channel
     * and retrieves representation of an instance of com.pi.conqweb.GetDailyTopChannels
     * @param     date_from input parameter in format yyyy-mm-dd
     *            size input parameter of the desired number of content items
     *            channelId ID of the retrieved instance (channel)
     * @return    an instance of java.lang.String.
     */
    public String getJson(@QueryParam("date_from") String date_from, @QueryParam("date_to") String date_to, @QueryParam("size") Integer size, @QueryParam("channelId") Integer channelId) {
        try {
            Gson gson = new Gson();
            ChartData data = new ChartData();
            String query = "";
            ChartMetadataColumn col1 = new ChartMetadataColumn();
            col1.index = 2;
            col1.colType = "int";
            List<ChartMetadataColumn> lista = new ArrayList<>();
            lista.add(col1);
            switch (ServerInstance.DefaultDatabaseType) {
                case CASSANDRA:
                    String channelWhere = "";
                    if (channelId != null) {
                        channelWhere = " and channel_id = " + channelId;
                    }
                    query = "select item_title, watch_count from daily_count_by_item" + " where log_date = '" + date_from + "'" + channelWhere + " limit " + size + " allow filtering;";
                    data = PiCassandraResultsetIterator.GetDefaultDataWithResultset(data, query, 1, 0, null);
                    break;
                case VERTICA:
                    String whereChannel = "";
                    if(channelId != null)
                        whereChannel = " and Content_Channel_Id = " + channelId;
                    query = "SELECT a.Content_item_Title, AVG(b.subscriberSum) AS avgSubsSum, b.csId from "
                        + "	(SELECT date , Content_Item_Id AS ciid, Content_Schedule_Id AS csId, sum(nosubscribers) as subscriberSum "
                        + "	FROM conq.agg_channel_daily   where date BETWEEN '" + date_from + "' AND '" + date_to + "' AND content_item_id IS NOT NULL " + whereChannel
                        + "	GROUP BY date, Content_Schedule_Id, Content_Item_Id ORDER BY subscriberSum desc) b  "
                        + " LEFT JOIN conq.Content_Item a ON a.Content_Item_Id = b.ciid  "
                        + " GROUP BY b.csId, a.Content_item_Title "
                        + " ORDER BY avgSubsSum DESC  LIMIT  " + size + "";
                    System.out.println(query);
                    data = PiVerticaResultsetIterator.GetDefaultDataWithResultset(data, query, 1, 0, lista);
                    break;
            }

            return gson.toJson(data);
        } catch (Exception ex) {
            Logger.getLogger(GetDailyTopItems.class.getName()).log(Level.SEVERE, null, ex);
            ResponseDTO response = new ResponseDTO("Server error", ex.getMessage(), true, null);
            return JsonBuilder.toJson(response);
        }
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
}
